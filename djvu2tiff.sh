#!/bin/bash

    pageStart=1 #с какой страницы начать конвертировать
    pageEnd=13 #до этой страницы будет конвертировать
    cd input
    for fileName in *.djvu; do
       fileExt=${fileName/*./''}
    done
    echo Конвертируем файл $fileName в рисунки tif...
    for ((pageNum=$pageStart; pageNum <= $pageEnd ; pageNum++))
      do  
        echo Конвертируеся страница $pageNum из $pageEnd...
        ddjvu -format=tiff -page=$pageNum -quality=100 $fileName ../output/${fileName%.$fileExt}$pageNum.tif
     done
