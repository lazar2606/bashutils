#!/bin/bash

#Commits. Each commit has an author and a committer field, which record who and when created the change and who committed it (Git is designed to work well with patches coming by mail - in that case, the author and the committer will be different). Git will try to guess your realname and email, but especially with email it is likely to get it wrong. You can check it using git config -l and set them with: 
git config --global user.name "Sergey Marchenko"
git config --global user.email sergey@mzsl.ru
#Colors. Git can produce colorful output with some commands; since some people hate colors way more than the rest likes them, by default the colors are turned off. If you would like to have colors in your output:
git config --global color.diff auto
git config --global color.status auto
git config --global color.branch auto
