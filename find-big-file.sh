#!/bin/bash
###############################################
# Find big files on the disk 
# example: $0 dirname
###############################################

WHERE=$@;
COMMAND="find ${WHERE} -size +500M";
echo ${COMMAND};
${COMMAND};
