#!/bin/bash

BLOCKER=$1
BLOCKED=$2

if [[ "$BLOCKED" == "" ]]; then
	echo "$0 <blocker> <blocked>"
	exit;
fi
# 1. Build new, blocked package first using --buildpkgonly --nodeps.
# 2. Only if that was successful do we unmerge the old, blocking package. (remove package withour depth)
# 3. Finally we install the new package with --usepkgonly
emerge --buildpkgonly --nodeps $BLOCKED && emerge -C $BLOCKER && emerge --usepkgonly $BLOCKED
