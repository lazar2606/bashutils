#!/bin/bash
#mencoder capture-4.avi -o capture-4_ppc.avi -oac copy -ovc lavc -vf crop=1024:752:0:10,scale=320:240  -lavcopts vcodec=mpeg4:vbitrate=400:mbd=2:v4mv:trell:vpass=1

USAGE="USAGE: mencoder.sh file.avi crop [exp: mencoder.sh file.avi 1024:752:0:10]\n";
VFILE=""
OUTFILE=""
CROP=""

if [ "x$1" = "x" ]; then
  echo -e $USAGE
  exit 1;
fi
if [ -f $1 ]; then
  VFILE=$1;
else
  echo -e "File $1 not found\n";
  exit 1;
fi

if [ "x$2" = "x" ]; then
  mplayer $VFILE -vf cropdetect -frames 40;
  printf "Set crop: ";
  read CROP
  if [ "x$CROP" = "x" ]; then
    echo -e "You must set crop parameter!\n";
    exit 1;
  fi
fi

OUTFILE=`echo $VFILE | sed "s/\./_ppc\./"`

echo "mencoder $VFILE -o $OUTFILE -oac copy -ovc lavc -vf crop=$CROP,scale=320:240  -lavcopts vcodec=mpeg4:vbitrate=400:mbd=2:v4mv:trell:vpass=1"