#!/bin/bash
# ------------------------------------------------------------------
# Show X message if battery level is too small 
# ------------------------------------------------------------------
CURRENT=$(acpi -b | awk '{print $4}' | tr -d '%,'); # battery level
MIN=5; # minimum battery level
TMP="/tmp/acpi-time.tmp"; # tmp for last show message time
TIME="`date +%s`"; #current time in seconds
LAST="0"; # last show message time
TIMEOUT=300; # in seconds

if [ -f $TMP ]; then
        LAST="`cat ${TMP}`";
fi

if [ $LAST == "" ]; then
        LAST="0";
fi
DIFF=$(($TIME - $LAST));

if [ "$DIFF" -lt "0" ]; then
        DIFF="0";
        rm $TMP;
fi

if [ "$CURRENT" -lt "$MIN" ] && [ "$DIFF" -gt "$TIMEOUT" ] ; then
       	echo $TIME > $TMP;
        xmessage -center -bg red "Low battery level: "$CURRENT" %";
fi
