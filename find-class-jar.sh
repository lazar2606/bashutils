#!/bin/bash
# ------------------------------------------------------------------
# Find class in jar files 
# ------------------------------------------------------------------

VERSION="1.0"
USAGE="Usage: $0 com/package/ClassName [directory path] or $0 com.package.ClassName [directory path]"


# --- Options processing -------------------------------------------
if [ $# == 0 ] ; then
    echo $USAGE
    exit 1;
fi

while getopts ":vh" optname
  do
    case "$optname" in
      "v")
        echo "Version $VERSION"
        exit 0;
        ;;
      "h")
        echo $USAGE
        exit 0;
        ;;
      "?")
        echo "Unknown option $OPTARG"
        exit 1;
        ;;
      ":")
        echo "No argument value for option $OPTARG"
        exit 1;
        ;;
      *)
        echo "Unknown error while processing options"
        exit 1;
        ;;
    esac
  done

shift $(($OPTIND - 1))


CLASS=${1//\./\/};
DIR="${2}";

if [ "${DIR}" == "" ]; then
        DIR="./"
fi

find $DIR -type f -name "*.jar" -print0 | xargs -0 -I '{}' sh -c 'jar tf {} | grep '$CLASS' && echo {}'
