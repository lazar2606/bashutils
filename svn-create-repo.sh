#!/bin/sh
##########################
#Create svn-repository
##########################
set -x

SVN_ROOT="/var/svn"
REPO=$1
mkdir -p $SVN_ROOT
cd $SVN_ROOT
svnadmin create $REPO
chown -R svn:svnusers "${SVN_ROOT}/${REPO}"
chmod -R g-w "${SVN_ROOT}/${REPO}"
chmod -R g+rw "${SVN_ROOT}/${REPO}/db"
chmod -R g+rw "${SVN_ROOT}/${REPO}/locks"
