#!/bin/bash
wget "http://downloads.sourceforge.net/project/xdxf/dicts-stardict-form-xdxf/001/stardict-comn_sdict02_eng_rus_full-2.4.2.tar.bz2?use_mirror=citylan"
tar -xjvf "stardict-comn_sdict02_eng_rus_full-2.4.2.tar.bz2"
sudo mv "stardict-comn_sdict02_eng_rus_full-2.4.2/*" "/usr/share/stardict/dic"
chmod a+rw  /usr/share/stardict/dic/*
rm -rf "stardict-comn_sdict02_eng_rus_full-2.4.2"
rm "stardict-comn_sdict02_eng_rus_full-2.4.2.tar.bz2"
