#!/bin/bash
# ------------------------------------------------------------------
# Find files which have been edited in the last days 
# ------------------------------------------------------------------

ADMIN_MAIL="lazar2606@gmail.com";
MAIL_SUBJECT="Report :: Files";
EXCLUDES_FILE="$2";
EXCL="";
DIR="$1";
REPORT="";
#Last days count
DAYS=1

if [ "$DIR" == "" ]; then
        echo "Search edited files.";
        echo "Usage: $0 /search/directory [/path/to/excludes.txt] [last days count]";
        echo "Example: $0 / /home/user/excludes.txt 3";
        echo "excludes.txt contains directories which must be ignored";
        exit 1;
fi

if [ "$3" != "" ]; then
        DAYS=$3
fi

if [ "$EXCLUDES_FILE" != "" ]; then
    for p in `cat ${EXCLUDES_FILE}`; do
            EXCL=" ${EXCL} -path ${p} -prune -o";
    done;
fi


RESULT=`find $1 ${EXCL} -mtime -${DAYS} -type f -print`;

if [ "$RESULT" != "" ]; then
        sendmail ${ADMIN_MAIL} <<EOF
subject:${MAIL_SUBJECT}
$RESULT
EOF
fi;

