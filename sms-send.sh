#!/bin/bash
# ------------------------------------------------------------------
# Send sms by gsm-modem 
# ------------------------------------------------------------------

m_phone_number=$1;
m_message=$2;
m_dev="/dev/ttyUSB0";


if [ "$m_phone_number" == "" ]; then
        echo -e "Need phone number\nTry: $0 --help\n";
        exit 100;
fi

if [ "$m_phone_number" == "--help" ]; then
        echo -e "Usage: $0 +7999999999 Testmessage\n";
        exit 0;
fi

if [ "$m_message" == "" ]; then
        echo -e "Need text of message\nTry: $0 --help\n";
        exit 101;
fi 

CHAT="`which chat`";

if [ $CHAT == "" ]; then
        echo -e "chat program not found!\nTry to check your permission or install 'ppp' package.";
        exit 102;
fi

function test {
    "$@"
    local status=$?
    if [ $status -ne 0 ]; then
#        echo "error with $1: code: $status" >&2
        exit $status;
    fi
    return $status
}

test chat -v -t 2 "" "AT" "OK" < $m_dev > $m_dev
test chat -v -t 2 "" "AT+CMGF=1" "OK" < $m_dev > $m_dev
test chat -v -t 2 "" "AT+CMGS=\"$m_phone_number\"" ">" < $m_dev > $m_dev
test chat -v -t 2 "" "$m_message" ">" < $m_dev > $m_dev
test chat -v -t 5 "" "^Z" "+CMGS: 1" < $m_dev > $m_dev
echo "\nOK\n";
