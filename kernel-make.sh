#!/bin/bash
#########################
# Rebuild Linux Kernel
#########################
set -x

KERNEL_NAME="linux-3.12.13-gentoo"
BUILD_NAME="Linux 3.12.13"

NEW_KERNEL_PATH="/boot/${KERNEL_NAME}"
GRUB_CONF="/boot/grub/grub.conf"

cd /usr/src/linux
make && make modules_install

if [ "f"`ls /boot/` == "f" ] ; then  
	mount /boot;
fi

cp arch/x86_64/boot/bzImage /boot/${KERNEL_NAME}

echo ""                                           >> $GRUB_CONF
echo "title ${BUILD_NAME}"                        >> $GRUB_CONF
echo "root (hd0,0)"            		          >> $GRUB_CONF
echo "kernel /boot/${KERNEL_NAME} root=/dev/sda3" >> $GRUB_CONF
echo ""                                           >> $GRUB_CONF

vim $GRUB_CONF
#rebuild nvidia modules use sys-kernel/module-rebuild
#module-rebuild populate <-- add nvidia module to database
/usr/sbin/module-rebuild rebuild
