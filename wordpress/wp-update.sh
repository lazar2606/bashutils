#!/bin/bash
set -x
. wp-update.cfg

rm -rf $WP_BACKUP;
cp -Rp $WP_ROOT $WP_BACKUP;

mkdir -p $(dirname ${WP_DB_DUMP});
mysqldump -u$MYSQL_USER -p$MYSQL_PASS $MYSQL_DB > $WP_DB_DUMP;

rm $WP_LATEST
mkdir -p $(dirname ${WP_LATEST});

cd $(dirname ${WP_LATEST});
wget $WP_VERSION;
tar -xzf ${WP_LATEST};

rm -rf $WP_ROOT"/*.php";
rm -rf $WP_ROOT"/wp-includes";
rm -rf $WP_ROOT"/wp-admin";
cp -Rp wordpress/* $WP_ROOT"/"
rm -rf wordpress
cp $WP_BACKUP"/wp-config.php" $WP_ROOT"/";
