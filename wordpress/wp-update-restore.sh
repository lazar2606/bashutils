#!/bin/bash
set -x
. wp-update.cfg

rm -rf $WP_ROOT;
cp -Rp $WP_BACKUP $WP_ROOT;
mysql -u${MYSQL_USER} -p${MYSQL_PASS} ${MYSQL_DB}  < $WP_DB_DUMP;
rm -rf $WP_BACKUP;

