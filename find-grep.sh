#!/bin/bash
# ------------------------------------------------------------------
# Find files which contains some text 
# ------------------------------------------------------------------

VERSION="1.0"
USAGE="Usage: $0 [directory where find] [what find] [grep text]"


# --- Options processing -------------------------------------------
if [ $# == 0 ] ; then
    echo $USAGE
    exit 1;
fi

while getopts ":vh" optname
  do
    case "$optname" in
      "v")
        echo "Version $VERSION"
        exit 0;
        ;;
      "h")
        echo $USAGE
        exit 0;
        ;;
      "?")
        echo "Unknown option $OPTARG"
        exit 1;
        ;;
      ":")
        echo "No argument value for option $OPTARG"
        exit 1;
        ;;
      *)
        echo "Unknown error while processing options"
        exit 1;
        ;;
    esac
  done

shift $(($OPTIND - 1))

WHERE=$1
FILE=$2
SEARCH=$3

# grep -H -- Print the filename for each match

find "$WHERE" -iname "$FILE" -exec grep -H "$SEARCH" --colour {}  \;

