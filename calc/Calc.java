package calc;
import java.lang.Double;
import java.lang.String;
import java.lang.StringBuilder;
import java.lang.System;

/**
 * Класс осуществляет арифметические операции
 * Такие как сложение, вычитание, умножение, деление.
 * Принцип работы:
 *  Сначало в строке выбираются выражения в скобках. Затем
 *  происходит парсинг этих выражений.
 * Пример использования:
 * java Calc (1+6)*9
 */
public class Calc {
    private final char SPACE = ' ';
    private final String OPERATIONS = "+-/*";
    private final String NUMBERS  = "0123456789";

	public static void main(String[] args) {
        if (args.length <= 0)
            return;
        StringBuilder sb = new StringBuilder("");
        String parse = null;
        for (int i = 0; i < args.length; i++) {
            sb.append(args[i]);
        }
        parse = sb.toString();
        //System.out.println("Выражение: " + parse);
        (new Calc()).parse(parse);
	}

    public String prepareString(String parse) {
        StringBuilder sb = new StringBuilder("");
        //удаляем все пробелы
        for (int i = 0; i < parse.length(); i++) {
            char c = parse.charAt(i);
            if (c != SPACE) {
                if (c == ',')
                    c = '.';
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public void parse(String parse) {
        parse = prepareString(parse);
        StringBuilder sb = new StringBuilder(parse);
        Double d = evaluteExpression(sb);
        if (d != null)
            System.out.println(d);
    }

    /**
     * Высисляет выражение 1 + 1 либо 2 + 5.1
     * @return
     */
    public Double evaluteExpression(StringBuilder sb) {
        String num1 = cutNumber(sb);
        if (num1 == null)
            return null;
        String op   = cutOperation(sb);
        if (op == null)
            return null;
        String num2 = cutNumber(sb);
        if (num2 == null)
            return null;
        Double result = 0d;
        Double n1 = Double.valueOf(num1);
        Double n2 = Double.valueOf(num2);
        if (op.equals("-")) {
            result = n1 - n2;
        } else if (op.equals("+")) {
            result = n1 + n2;
        } else if (op.equals("/")) {
            result = n1 / n2;
        } else if (op.equals("*")) {
            result = n1 * n2;
        }
        return result;
    }

    public void printError(String error) {
        System.out.println(error);
    }

    public String cutOperation(StringBuilder sb) {
        for (int i = 0; i < sb.length(); i++) {
            char c = sb.charAt(i);
            //если c это операция
            if (OPERATIONS.indexOf(c) >= 0) {
                sb.deleteCharAt(i);
                return String.valueOf(c);
            } else {
                printError("Ошибка: Калькулятор поддерживает только следующие операции: " + OPERATIONS);
                break;
            }
        }
        return null;
    }

    public String cutNumber(StringBuilder str) {
        StringBuilder sb = new StringBuilder("");
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            //собираем число по символам
            if ( isDigit(c) || isDelim(c) ) {
                sb.append(c);
                str.deleteCharAt(i);
                i = i - 1;
            } else {
                break;
            }
        }
        String result = sb.toString();
        if (result == null || result.isEmpty()) {
            printError("Не удалось найти число!");
            return  null;
        }
        return result;
    }

    public boolean isDigit(char c) {
        return (NUMBERS.indexOf(c) >= 0);
    }

    public boolean isDelim(char c) {
        return (c == '.' || c == ',');
    }
}
