#!/bin/bash

######################
# Скрипт для конвертирования видео с камеры
# с формата mov в avi
######################

bn=`basename "$1"`
dn=`dirname "$1"`
file=${bn%.MOV} #базовое имя файла

file_mov="${file}.MOV" # файл с расширением MOV
full_file_mov="${dn}/${file_mov}"
file_avi="${file}.AVI" # файл с расширением avi
full_file_avi=${dn}/${file_avi} # полный путь к файлу avi
#echo $bn
#echo $file
echo ${dn}/${file}.avi

#mencoder ${full_file_mov} -o ${full_file_avi} -oac mp3lame -ovc x264
ffmpeg -i ${full_file_mov} -acodec pcm_s16le -b 5000k -vcodec mpeg4 -vtag XVID ${full_file_avi}
