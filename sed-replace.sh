#!/bin/bash

FILE=$1;
TMP=$FILE".tmp"

sed -e 's/<li>//g' $FILE > $TMP && mv $TMP $FILE 
sed -e 's/<\/li>//g' $FILE > $TMP && mv $TMP $FILE
sed -e 's/&lt;/</g' $FILE > $TMP && mv $TMP $FILE 
sed -e 's/&gt;/>/g' $FILE > $TMP && mv $TMP $FILE
sed -e 's/&amp;/\&/g' $FILE > $TMP && mv $TMP $FILE
sed -e 's/^\s*//g' $FILE  > $TMP && mv $TMP $FILE
sed -e 's/&quot;/"/g' $FILE  > $TMP && mv $TMP $FILE
# удаляем пробелы и табуляции с начала каждой строки
# весь текст выравнивается по левому краю
sed -e 's/^[ \t]*//' $FILE > $TMP && mv $TMP $FILE
# удаляем пробелы и табуляции в конце каждой строки
sed -e 's/[ \t]*$//' $FILE > $TMP && mv $TMP $FILE
