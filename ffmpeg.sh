#!/bin/bash
set -x

INPUT=$1
OUTPUT="${INPUT%.*}.webm"
/usr/bin/ffmpeg -i $INPUT -threads 4 -qmax 40 -acodec libvorbis -ab 128k -ar 41000 -vcodec libvpx -y $OUTPUT
