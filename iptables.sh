#!/bin/sh

#######################################
# Скрипт для настройки iptables
# Базовые действия:
# ACCEPT   - пропустить
# DROP     - заблокировать
# CONTINUE - продолжить обработку в данной цепочке
# RETURN   - прекратить обработку в данной цепочке,
#            вернуть в цепочку уровнем выше

#######################################


#######################################
# Переменные:
#######################################
# Путь к iptables
IPTABLES="/sbin/iptables"
# Интерфейс через который осуществляется подключение к интернету (internet interface)
INIF="eth0"
PPP0="ppp0"
# Интерфейс через который подключена локальная (домашняя сеть)
LOCIF="eth1"
LOCAL_NET="192.168.0.0/24"

#######################################
echo "1" > /proc/sys/net/ipv4/ip_forward

echo "1" > /proc/sys/net/ipv4/tcp_syncookies

echo "1" > /proc/sys/net/ipv4/icmp_echo_ignore_broadcasts

echo "1" > /proc/sys/net/ipv4/icmp_ignore_bogus_error_responses

#необходим для корректной работы FTP-сервера
#/sbin/modprobe ip_conntrack_ftp

#Сбрасываем старые данные и правила
${IPTABLES} -F
${IPTABLES} -F -t nat
${IPTABLES} -F -t mangle
${IPTABLES} -X
${IPTABLES} -X -t nat
${IPTABLES} -X -t mangle

echo "Old Rules Flushed"


#Устанавливаем правила на DROP по-умолчанию. Т.е. блокируем все пакеты на вход и выход (Set Default-Drop Policy)
${IPTABLES} -P INPUT DROP
${IPTABLES} -P OUTPUT DROP
${IPTABLES} -P FORWARD DROP

#Create New Chain Called BAD_PACKETS
${IPTABLES} -N BAD_PACKETS

#Разрешаем работу локального интерфейса, того самого 127.0.0.1 (Allow The Loopback)
${IPTABLES} -A INPUT -i lo -j ACCEPT
#Разрешаем все с локальной сети
${IPTABLES} -A INPUT -s ${LOCAL_NET} -i ${LOCIF} -j ACCEPT

#Jump To BAD_PACKETS
${IPTABLES} -A INPUT -j BAD_PACKETS

${IPTABLES} -A OUTPUT -d 192.168.1.0/24 -j DROP
${IPTABLES} -A OUTPUT -o eth1 -d 192.168.1.0/24 -j DROP

#Разрешаем поддерживать открытыми уже установленные соединения (Allow Established Connections)
#${IPTABLES} -A INPUT -i ${INIF} -m state --state ESTABLISHED,RELATED -j ACCEPT
#${IPTABLES} -A INPUT -i ${PPP0} -m state --state ESTABLISHED,RELATED -j ACCEPT
${IPTABLES} -A INPUT -i ${INIF} -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
${IPTABLES} -A INPUT -i ${PPP0} -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
# Разрешаем получать данные от DHCP-сервера. (Allow DHCP)
${IPTABLES} -A INPUT -p UDP --dport 68 --sport 67 -j ACCEPT

${IPTABLES} -A INPUT -p TCP -i ${PPP0} --dport 65000 -j ACCEPT
##########################
#Разрешаем присоединяться к SSH (используем нестандартный порт)
${IPTABLES} -A INPUT -p TCP -i ${PPP0} --dport 2222 -j ACCEPT
##########################
# Открытие порта по стуку на другой порт (сделано для ssh)
#l ${IPTABLES} -N sshguard
# Если адрес в списке, то подключение разрешено
#l ${IPTABLES} -A sshguard -m state --state NEW -m recent --rcheck --name SSH -j ACCEPT
# Разрешить пакеты для уже установленных соединений
#l ${IPTABLES} -A sshguard -m state --state ESTABLISHED,RELATED -j ACCEPT
#l ${IPTABLES} -A sshguard -j DROP


# Заносит адрес в список
#l ${IPTABLES} -A INPUT -m state -i ${PPP0} --state NEW -m tcp -p tcp --dport 2222 -j LOG --log-prefix "SSH-open: "
#l ${IPTABLES} -A INPUT -m state -i ${PPP0} --state NEW -m tcp -p tcp --dport 2222 -m recent --name SSH --set -j DROP
# Удаляет адрес из списка
#l ${IPTABLES} -A INPUT -m state -i ${PPP0} --state NEW -m tcp -p tcp --dport 2223 -m recent --name SSH --remove -j DROP
 
# Применение для трафика ssh цепочку sshguard
#l ${IPTABLES} -A INPUT -p TCP -i ${PPP0} --dport 22 -j sshguard
#${IPTABLES} -A INPUT -p TCP -i ${PPP0} --dport 22 -j LOG --log-prefix "SSH-OPEN: "
#############################


##########################
#Разрешаем присоединяться к WEBLOGIC
##########################
# Открытие порта по стуку на другой порт
#${IPTABLES} -N weblogic
# Если адрес в списке, то подключение разрешено
#${IPTABLES} -A weblogic -m state --state NEW -m recent --rcheck --name WEBLOG -j ACCEPT
# Разрешить пакеты для уже установленных соединений
#${IPTABLES} -A weblogic -m state --state ESTABLISHED,RELATED -j ACCEPT
#${IPTABLES} -A weblogic -j DROP

# Заносит адрес в список
#${IPTABLES} -A INPUT -m state -i ${PPP0} --state NEW -m tcp -p tcp --dport 8107 -j LOG --log-prefix "WEBLOGIC-open: "
#${IPTABLES} -A INPUT -m state -i ${PPP0} --state NEW -m tcp -p tcp --dport 8107 -m recent --name WEBLOG --set -j DROP
# Удаляет адрес из списка
#${IPTABLES} -A INPUT -m state -i ${PPP0} --state NEW -m tcp -p tcp --dport 8108 -m recent --name WEBLOG --remove -j DROP

# Применение для трафика weblogic цепочку weblogic
#${IPTABLES} -A INPUT -p TCP -i ${PPP0} --dport 8106 -j weblogic
#############################





# DNS TCP
${IPTABLES} -A INPUT -p TCP -i ${INIF} --dport 53 -j ACCEPT
# DNS UDP
${IPTABLES} -A INPUT -p UDP -i ${INIF} --dport 53 -j ACCEPT

#Разрешаем присоединяться к серверу приложений
#${IPTABLES} -A INPUT -p TCP -i ${PPP0} --dport 8106 -j ACCEPT
#${IPTABLES} -A INPUT -p TCP -i ${INIF} --dport 8106 -j ACCEPT

#Открываем порты для работы L2TP и PPTP
${IPTABLES} -A INPUT -p TCP -i ${INIF} --dport 1701 -j ACCEPT
${IPTABLES} -A INPUT -p UDP -i ${INIF} --dport 1701 -j ACCEPT
${IPTABLES} -A INPUT -p TCP -i ${INIF} --dport 1723 -j ACCEPT
${IPTABLES} -A INPUT -p UDP -i ${INIF} --dport 1723 -j ACCEPT

#Разрешаем сетевую печать на этом компьютере. (Allow net print)
#${IPTABLES} -A INPUT -p UDP -i ${INIF} --dport 631 -j ACCEPT

#Разрешаем webmin на стандартном 10000 порту (Allow webmin)
#${IPTABLES} -A INPUT -p TCP -i ${INIF} --dport 10000 -j ACCEPT

#Allow avahi-daemon
#${IPTABLES} -A INPUT -p TCP -i ${INIF} --dport 5353 -j ACCEPT

#Allow input skype
${IPTABLES} -A INPUT -p TCP -i ${PPP0} --dport 13308 -j ACCEPT

#${IPTABLES} -A INPUT -p TCP -i ${PPP0} --dport 8080 -j ACCEPT

#Allow input torrents-client
${IPTABLES} -A INPUT -p TCP -i ${PPP0} --dport 51413 -j ACCEPT
${IPTABLES} -A INPUT -p UDP -i ${PPP0} --dport 51413 -j ACCEPT
${IPTABLES} -A INPUT -p TCP -i ${PPP0} --dport 6881 -j ACCEPT
${IPTABLES} -A INPUT -p UDP -i ${PPP0} --dport 6881 -j ACCEPT

${IPTABLES} -A INPUT -p TCP -i ${INIF} --dport 51413 -j ACCEPT
${IPTABLES} -A INPUT -p UDP -i ${INIF} --dport 51413 -j ACCEPT
${IPTABLES} -A INPUT -p TCP -i ${INIF} --dport 6881 -j ACCEPT
${IPTABLES} -A INPUT -p UDP -i ${INIF} --dport 6881 -j ACCEPT

${IPTABLES} -A INPUT -p UDP -i ${PPP0} --dport 4444 -j ACCEPT
${IPTABLES} -A INPUT -p UDP -i ${INIF} --dport 4444 -j ACCEPT

${IPTABLES} -A INPUT -p TCP -i ${PPP0} --dport 80  -j ACCEPT

#Allow Samba From Specified Hosts
#${IPTABLES} -A INPUT -p TCP -i ${INIF} --dport 137:139 -j ACCEPT
#${IPTABLES} -A INPUT -p UDP -i ${INIF} --dport 137:139 -j ACCEPT
#${IPTABLES} -A INPUT -p TCP -i ${INIF} --sport 137:139 -j ACCEPT
#${IPTABLES} -A INPUT -p UDP -i ${INIF} --sport 137:139 -j ACCEPT

#Allow ICMP Replies From Specified Hosts (Ping)
#${IPTABLES} -A INPUT -p ICMP -i ${PPP0} --icmp-type 8 -j ACCEPT
#${IPTABLES} -A INPUT -p icmp --icmp-type 8 -j ACCEPT
#${IPTABLES} -A INPUT -p icmp --icmp-type 11 -j ACCEPT
${IPTABLES} -A INPUT -p icmp -j ACCEPT
#${IPTABLES} -A INPUT -p ICMP -i ${INIF} --icmp-type 8 -j ACCEPT

#Разрешаем подключаться к SMTP на 587 порт, как это настроить указано тут. (Allow input mail on 587 port)
#${IPTABLES} -A INPUT -p tcp --dport 587 -j ACCEPT

#Log
#${IPTABLES} -A INPUT -j LOG --log-prefix "INPUT DROP: "

#Accept Loopback On OUTPUT
${IPTABLES} -A OUTPUT -o lo -j ACCEPT

#Разрешаем все с локальной сети
#${IPTABLES} -t filter -A FORWARD -p tcp -s ${LOCAL_NET} -d 217.20.147.94  --dport 80 -j DROP
#${IPTABLES} -t filter -A FORWARD -p tcp -s ${LOCAL_NET} -d 89.249.21.219  --dport 80 -j DROP
#${IPTABLES} -t filter -A FORWARD -p tcp -s ${LOCAL_NET} -d 87.240.131.120 --dport 80 -j DROP

${IPTABLES} -A OUTPUT -d ${LOCAL_NET} -o ${LOCIF} -j ACCEPT

#Разрешаем поддерживать открытыми уже установленные соединения. (Allow Established Connections)
#${IPTABLES} -A OUTPUT -o ${PPP0} --match state --state NEW,ESTABLISHED -j ACCEPT
#${IPTABLES} -A OUTPUT -o ${INIF} --match state --state NEW,ESTABLISHED -j ACCEPT
${IPTABLES} -A OUTPUT -o ${PPP0} --match conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
${IPTABLES} -A OUTPUT -o ${INIF} --match conntrack --ctstate NEW,ESTABLISHED -j ACCEPT

#Разрешить получать IP по DHCP (Allow DHCP)
${IPTABLES} -A OUTPUT -p UDP --dport 67 --sport 68 -j ACCEPT


#Открываем порты для weblogic
${IPTABLES} -A OUTPUT -p TCP -o ${PPP0} --dport 8106 -j ACCEPT

#Открываем порты для работы L2TP и PPTP
${IPTABLES} -A OUTPUT -p TCP -o ${INIF} --dport 1701 -j ACCEPT
${IPTABLES} -A OUTPUT -p UDP -o ${INIF} --dport 1701 -j ACCEPT
${IPTABLES} -A OUTPUT -p TCP -o ${PPP0} --dport 1723 -j ACCEPT
${IPTABLES} -A OUTPUT -p UDP -o ${PPP0} --dport 1723 -j ACCEPT

#Allow HTTP,FTP,DNS,SSH, SMTP & Port 443 Outbound
${IPTABLES} -A OUTPUT -p TCP -o ${PPP0} --dport 443 -j ACCEPT
${IPTABLES} -A OUTPUT -p TCP -o ${PPP0} --dport 80 -j ACCEPT
${IPTABLES} -A OUTPUT -p TCP -o ${INIF} --dport 53 -j ACCEPT
${IPTABLES} -A OUTPUT -p UDP -o ${INIF} --dport 53 -j ACCEPT
${IPTABLES} -A OUTPUT -p TCP -o ${PPP0} --dport 25 -j ACCEPT
${IPTABLES} -A OUTPUT -p TCP -o ${PPP0} --dport 22 -j ACCEPT
${IPTABLES} -A OUTPUT -p TCP -o ${PPP0} --dport 21 -j ACCEPT
#Allow connect to gmail smtp
${IPTABLES} -A OUTPUT -p TCP -o ${PPP0} --dport 587 -j ACCEPT

#Позволять работать с sygma.ru по ssh
${IPTABLES} -A OUTPUT -p TCP -o ${PPP0} --dport 2567 -j ACCEPT

#Allow output skype
${IPTABLES} -A OUTPUT -p TCP -o ${PPP0} --dport 13308 -j ACCEPT
${IPTABLES} -A OUTPUT -p UDP -o ${PPP0} --sport 13308 -j ACCEPT

#Allow pop, imap
#${IPTABLES} -A OUTPUT -p TCP -o ${INIF} --dport 110 -j ACCEPT
#${IPTABLES} -A OUTPUT -p TCP -o ${INIF} --dport 143 -j ACCEPT

#Allow IMAPS
#${IPTABLES} -A OUTPUT -p TCP -o ${INIF} --dport 993 -j ACCEPT

#Allow output icq
${IPTABLES} -A OUTPUT -p TCP -o ${PPP0} --dport 5190 -j ACCEPT

#Разрешить выход на IRC. (Allow output IRC)
#${IPTABLES} -A OUTPUT -p TCP -o ${INIF} --dport 6667 -j ACCEPT
#${IPTABLES} -A OUTPUT -p TCP -o ${INIF} --dport 6668 -j ACCEPT
#${IPTABLES} -A OUTPUT -p TCP -o ${INIF} --dport 6669 -j ACCEPT
#Netprint port for upload book
${IPTABLES} -A OUTPUT -p TCP -o ${INIF} --dport 8080 -j ACCEPT


#Allow output Google talk
#${IPTABLES} -A OUTPUT -p TCP -o ${INIF} --dport 5222 -j ACCEPT

#Allow output CUPS (for printers in net)
#${IPTABLES} -A OUTPUT -p UDP -o ${INIF} --dport 631 -j ACCEPT
 
#Allow output avahi-daemon
#${IPTABLES} -A OUTPUT -p UDP -o ${INIF} --dport 5353 -j ACCEPT

#Allow output teamviewer
#${IPTABLES} -A OUTPUT -p UDP -o ${INIF} --dport 5938 -j ACCEPT

#Allow output ntp (for ntpdate)
#${IPTABLES} -A OUTPUT -p UDP -o ${INIF} --dport 123 -j ACCEPT

#Allow output Urban Terror
#${IPTABLES} -A OUTPUT -p UDP -o ${INIF} --dport 27960 -j ACCEPT

#Allow specify ports
#${IPTABLES} -A OUTPUT -p TCP -o ${INIF} --dport 2046 -j ACCEPT
#${IPTABLES} -A OUTPUT -p TCP -o ${INIF} --dport 2050 -j ACCEPT

#Allow Samba From Specified Hosts
#${IPTABLES} -A OUTPUT -p TCP -o ${INIF} --dport 137:139 -j ACCEPT
#${IPTABLES} -A OUTPUT -p UDP -o ${INIF} --dport 137:139 -j ACCEPT
#${IPTABLES} -A OUTPUT -p TCP -o ${INIF} --dport 445 -j ACCEPT
#${IPTABLES} -A OUTPUT -p UDP -o ${INIF} --dport 445 -j ACCEPT

#Allow ICMP Replies (Ping)
#${IPTABLES} -A OUTPUT -p icmp --icmp-type 8 -j ACCEPT
#${IPTABLES} -A OUTPUT -p icmp --icmp-type 11 -j ACCEPT
${IPTABLES} -A OUTPUT -p icmp -j ACCEPT
#Drop
#${IPTABLES} -A OUTPUT -j LOG --log-prefix "OUTPUT DROP: "


############################
# Блокируем сайты с рекламой
#${IPTABLES} -A OUTPUT -p tcp -d 31.13.64.48 -j DROP

############################
#forward пакетов
#включаем фрагментацию пакетов(из-за разницы mtu у интерфейсов eth0 и ppp0)
${IPTABLES} -A FORWARD -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu
${IPTABLES} -A FORWARD -i ${LOCIF} -p icmp -j ACCEPT
#разрешаем все пакеты из локальной сети
${IPTABLES} -A FORWARD -i ${LOCIF} -j ACCEPT

# разрешаем пакеты извне в локальную сеть только для уже установленных соединений
#${IPTABLES} -A FORWARD -s ${LOCAL_NET} -o ${PPP0} -j ACCEPT
#${IPTABLES} -A FORWARD -d ${LOCAL_NET} -m state --state ESTABLISHED,RELATED -i ${PPP0} -j ACCEPT
#${IPTABLES} -A FORWARD -d ${LOCAL_NET} -m state --state ESTABLISHED,RELATED -i ${INIF} -j ACCEPT
${IPTABLES} -A FORWARD -d ${LOCAL_NET} -m conntrack --ctstate ESTABLISHED,RELATED -i ${PPP0} -j ACCEPT
${IPTABLES} -A FORWARD -d ${LOCAL_NET} -m conntrack --ctstate ESTABLISHED,RELATED -i ${INIF} -j ACCEPT
${IPTABLES} -A FORWARD -p icmp -j ACCEPT

${IPTABLES} -t nat -A POSTROUTING -s ${LOCAL_NET} -o ${PPP0} -j MASQUERADE
${IPTABLES} -t nat -A POSTROUTING -s ${LOCAL_NET} -o ${INIF} -j MASQUERADE
#Перенаправляем ssh с 22-ого порта
#${IPTABLES} -t nat -A PREROUTING -i ${INIF} -p tcp --dport 777 -j REDIRECT --to-port 22
#${IPTABLES} -t nat -A PREROUTING -i ${PPP0} -p tcp --dport 777 -j REDIRECT --to-port 22


# Don't forward from the outside to the inside.
${IPTABLES} -A FORWARD -i ${PPP0} -o ${PPP0} -j REJECT
${IPTABLES} -A FORWARD -i ${INIF} -o ${INIF} -j REJECT

#${IPTABLES} -A BAD_PACKETS -p TCP ! --syn -m state --state NEW -j DROP
${IPTABLES} -A BAD_PACKETS -p TCP ! --syn -m conntrack --ctstate NEW -j DROP
${IPTABLES} -A BAD_PACKETS -p TCP --tcp-flags ALL ALL -j DROP
${IPTABLES} -A BAD_PACKETS -p TCP --tcp-flags ALL NONE -j DROP
#${IPTABLES} -A BAD_PACKETS -p TCP --tcp-flags ALL SYN \-m state --state ESTABLISHED -j DROP
${IPTABLES} -A BAD_PACKETS -p TCP --tcp-flags ALL SYN \-m conntrack --ctstate ESTABLISHED -j DROP
${IPTABLES} -A BAD_PACKETS -p ICMP --fragment -j DROP
#${IPTABLES} -A BAD_PACKETS -m state --state INVALID -j DROP
${IPTABLES} -A BAD_PACKETS -m conntrack --ctstate INVALID -j DROP
${IPTABLES} -A BAD_PACKETS -d 255.255.255.255 -j DROP
${IPTABLES} -A BAD_PACKETS -j RETURN

echo "Rules written."
