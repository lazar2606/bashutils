#!/bin/bash
set -x
#####################
# Update distr on gentoo
#####################
# Portage will then search for newer version of the applications you have installed. However, it will only verify the versions for the applications you have explicitly installed (the applications listed in /var/lib/portage/world) - it does not thoroughly check their dependencies. If you want to update the dependencies of those packages as well, add the --deep argument: 
# Still, this doesn't mean all packages: some packages on your system are needed during the compile and build process of packages, but once that package is installed, these dependencies are no longer required. Portage calls those build dependencies. To include those in an update cycle, add --with-bdeps=y:
emerge --sync
# emerge --pretend --update --deep --with-bdeps=y world
emerge --pretend --update --deep --with-bdeps=y @world
