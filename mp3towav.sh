#!/bin/bash
# ------------------------------------------------------------------
# Converting mp3 file to wav (for arduino player) 
# ------------------------------------------------------------------

#set -x;

FROM=$1;
TO=$2;
echo "Converting  $FROM $TO ...";

if [ "_${FROM}" == "_" ]; then
        echo -e "Usage: $0 FROM SRC";
        echo -e "Converting mp3 file to wav";
        exit;
fi
if [ "_${TO}" == "_" ]; then
        echo -e "Usage: $0 FROM SRC";
        echo -e "Converting mp3 file to wav";
        exit;
fi

mpg123 -w "_$TO" "$FROM";
sox -S "_$TO" -r 16000 -b 8 -c 1 -e unsigned-integer "$TO";
rm -f "_$TO";
